<?php

/**
 * Implementation of hook_strongarm().
 */
function ambiente_attribuzione_automatica_strongarm() {
  $export = array();
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'multistep_extra_workflow_group';
  $strongarm->value = '0';

  $export['multistep_extra_workflow_group'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'workflow_group';
  $strongarm->value = array(
    '0' => 'node',
  );

  $export['workflow_group'] = $strongarm;
  return $export;
}
